﻿using System;
using System.Numerics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1._1
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] lines = System.IO.File.ReadAllLines(@"..\..\input");

            string[] str = lines[0].Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            BigInteger[] m = new BigInteger[str.Length];
            for (int i = 0; i < str.Length; i++)
            {
                m[i] = BigInteger.Parse(str[i]);
            }

            str = lines[1].Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            BigInteger[] a = new BigInteger[str.Length];
            for (int i = 0; i < str.Length; i++)
            {
                a[i] = BigInteger.Parse(str[i]);
            }

            BigInteger x = 0;

            BigInteger c = 1;
            BigInteger s = a[0] % m[1];

            for (int i = 1; i < m.Length; i++)
            {
                c = c * m[i - 1];
                BigInteger d = Extended_GCD(c, m[i])[1];
                if (d < 0) d = d + m[i];

                x = (d * (a[i] - s)) % m[i];
                if (x < 0) x = x + m[i];
                s = s + x * c;
            }

            Console.WriteLine(s);
        }

        public static BigInteger[] Extended_GCD(BigInteger A, BigInteger B)
        {
            BigInteger[] result = new BigInteger[3];
            bool reverse = false;
            if (A < B)
            {
                BigInteger temp = A;
                A = B;
                B = temp;
                reverse = true;
            }
            BigInteger r = B;
            BigInteger q = 0;
            BigInteger x0 = 1;
            BigInteger y0 = 0;
            BigInteger x1 = 0;
            BigInteger y1 = 1;
            BigInteger x = 0, y = 0;
            while (A % B != 0)
            {
                r = A % B;
                q = A / B;
                x = x0 - q * x1;
                y = y0 - q * y1;
                x0 = x1;
                y0 = y1;
                x1 = x;
                y1 = y;
                A = B;
                B = r;
            }
            result[0] = r;
            if (reverse)
            {
                result[1] = y;
                result[2] = x;
            }
            else
            {
                result[1] = x;
                result[2] = y;
            }
            return result;
        }
    }
}
