﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("A = ");
            BigInt A = new BigInt(Console.ReadLine());

            Console.Write("B = ");
            BigInt B = new BigInt(Console.ReadLine());

            Console.Write("P = ");
            BigInt P = new BigInt(Console.ReadLine());

            Console.Write("Одноразрядное число n = ");
            int n = int.Parse(Console.ReadLine());

            Console.WriteLine("A + B = " + (A + B));
            Console.WriteLine("A - B = " + (A - B));
            Console.WriteLine("A * B = " + (A * B));
            Console.WriteLine("A / n = " + (A / n));
            Console.WriteLine("A % n = " + (A % n));
            Console.WriteLine("A / B = " + (A / B));
            Console.WriteLine("A % B = " + (A % B));
            Console.WriteLine("A ^ B = " + BigInt.ModPow(A, B, P));
        }
    }
}
