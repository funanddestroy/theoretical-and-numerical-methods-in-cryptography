﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3
{
    class BigInt
    {
        private List<int> value = new List<int>();
        private static int b = 10;
        private bool sign = true;

        public void ChangeSign(bool newSign)
        {
            sign = newSign;
        }

        public BigInt(string s)
        {
            if (s.Length < 1)
            {
                throw new FormatException("Введена пустая строка.");
            }

            sign = true;

            for (int i = s.Length - 1; i >= 0; i--)
            {
                if (i == 0 && s[0] == '-')
                {
                    sign = false;
                    break;
                }

                if (s[i] < '0' || s[i] > '9')
                {
                    throw new FormatException("Есть недопустимые значения в числе.");
                }

                int tmpVal = int.Parse(s[i].ToString());
                this.value.Add(tmpVal);
            }
        }

        public BigInt(BigInt bigInt)
        {
            this.sign = bigInt.sign;
            for (int i = 0; i < bigInt.value.Count; i++)
            {
                this.value.Add(bigInt.value[i]);
            }
        }

        public BigInt(int[] array, bool sign)
        {
            if (array.Length < 1)
            {
                throw new FormatException("Введена пустая строка.");
            }

            this.value = normalize(new List<int>(array));
            this.sign = sign;
        }

        public BigInt(List<int> val, bool sign)
        {
            this.value = normalize(val);
            this.sign = sign;
        }

        public int GetLength()
        {
            return value.Count;
        }

        public int GetBase()
        {
            return b;
        }

        public override string ToString()
        {
            StringBuilder strValue = new StringBuilder();

            if (!sign)
            {
                strValue.Append('-');
            }

            for (int i = value.Count - 1; i >= 0; i--)
            {
                strValue.Append(value[i]);
            }

            return strValue.ToString();
        }

        private static List<int> normalize(List<int> val)
        {
            while (val.Count > 1 && val[val.Count - 1] == 0)
            {
                val.RemoveAt(val.Count - 1);
            }
            return val;
        }

        public static bool operator >(BigInt bigIntFirst, BigInt bigIntSecond)
        {
            if (bigIntFirst.sign && !bigIntSecond.sign)
            {
                return true;
            }

            if (!bigIntFirst.sign && bigIntSecond.sign)
            {
                return false;
            }

            if (bigIntFirst.value.Count == bigIntSecond.value.Count)
            {
                if (bigIntFirst.sign)
                {
                    bool f = false;
                    for (int i = 0; i < bigIntFirst.value.Count; i++)
                    {
                        if (bigIntFirst.value[i] > bigIntSecond.value[i])
                        {
                            f = true;
                        }
                        if (bigIntFirst.value[i] < bigIntSecond.value[i])
                        {
                            f = false;
                        }
                    }
                    return f;
                }
                else
                {
                    bool f = false;
                    for (int i = 0; i < bigIntFirst.value.Count; i++)
                    {
                        if (bigIntFirst.value[i] < bigIntSecond.value[i])
                        {
                            f = true;
                        }
                        if (bigIntFirst.value[i] > bigIntSecond.value[i])
                        {
                            f = false;
                        }
                    }
                    return f;
                }
            }

            if (bigIntFirst.value.Count > bigIntSecond.value.Count)
            {
                if (bigIntFirst.sign)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            if (bigIntFirst.sign)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public static bool operator <(BigInt bigIntFirst, BigInt bigIntSecond)
        {
            if (bigIntFirst.sign && !bigIntSecond.sign)
            {
                return false;
            }

            if (!bigIntFirst.sign && bigIntSecond.sign)
            {
                return true;
            }

            if (bigIntFirst.value.Count == bigIntSecond.value.Count)
            {
                if (bigIntFirst.sign)
                {
                    bool f = false;
                    for (int i = 0; i < bigIntFirst.value.Count; i++)
                    {
                        if (bigIntFirst.value[i] < bigIntSecond.value[i])
                        {
                            f = true;
                        }
                        if (bigIntFirst.value[i] > bigIntSecond.value[i])
                        {
                            f = false;
                        }
                    }
                    return f;
                }
                else
                {
                    bool f = false;
                    for (int i = 0; i < bigIntFirst.value.Count; i++)
                    {
                        if (bigIntFirst.value[i] > bigIntSecond.value[i])
                        {
                            f = true;
                        }
                        if (bigIntFirst.value[i] < bigIntSecond.value[i])
                        {
                            f = false;
                        }
                    }
                    return f;
                }
            }

            if (bigIntFirst.value.Count > bigIntSecond.value.Count)
            {
                if (bigIntFirst.sign)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }

            if (bigIntFirst.sign)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool operator ==(BigInt bigIntFirst, BigInt bigIntSecond)
        {
            if ((bigIntFirst.sign && bigIntSecond.sign || !bigIntFirst.sign && !bigIntSecond.sign)
                && bigIntFirst.value.Count == bigIntSecond.value.Count)
            {
                for (int i = 0; i < bigIntFirst.value.Count; i++)
                {
                    if (bigIntFirst.value[i] != bigIntSecond.value[i])
                    {
                        return false;
                    }
                }

                return true;
            }

            return false;
        }

        public static bool operator !=(BigInt bigIntFirst, BigInt bigIntSecond)
        {
            return !(bigIntFirst == bigIntSecond);
        }

        public static bool operator >=(BigInt bigIntFirst, BigInt bigIntSecond)
        {
            return (bigIntFirst > bigIntSecond) || (bigIntFirst == bigIntSecond);
        }

        public static bool operator <=(BigInt bigIntFirst, BigInt bigIntSecond)
        {
            return (bigIntFirst < bigIntSecond) || (bigIntFirst == bigIntSecond);
        }

        public static BigInt operator +(BigInt bigIntFirst, BigInt bigIntSecond)
        {
            if (!bigIntFirst.sign && bigIntSecond.sign)
            {
                BigInt tmp = new BigInt(bigIntFirst.value, true);
                return bigIntSecond - tmp;
            }

            if (bigIntFirst.sign && !bigIntSecond.sign)
            {
                BigInt tmp = new BigInt(bigIntSecond.value, true);
                return bigIntFirst - tmp;
            }

            bool sumSign = true;

            if (!bigIntFirst.sign && !bigIntSecond.sign)
            {
                sumSign = false;
            }

            int k = 0;
            int n = Math.Max(bigIntFirst.value.Count, bigIntSecond.value.Count);

            List<int> sum = new List<int>();

            for (int i = 0; i < n; i++)
            {
                int tmpA = (bigIntFirst.value.Count > i) ? bigIntFirst.value[i] : 0;
                int tmpB = (bigIntSecond.value.Count > i) ? bigIntSecond.value[i] : 0;
                sum.Add(tmpA + tmpB + k);
                if (sum[i] >= b)
                {
                    sum[i] -= b;
                    k = 1;
                }
                else
                {
                    k = 0;
                }
            }

            if (k == 1)
            {
                sum.Add(k);
            }

            return new BigInt(sum, sumSign);
        }

        public static BigInt operator -(BigInt bigIntFirst, BigInt bigIntSecond)
        {
            if (bigIntFirst == bigIntSecond)
            {
                return new BigInt("0");
            }

            if (!bigIntFirst.sign && bigIntSecond.sign)
            {
                BigInt tmp = new BigInt(bigIntSecond.value, false);
                return bigIntFirst + tmp;
            }

            if (bigIntFirst.sign && !bigIntSecond.sign)
            {
                BigInt tmp = new BigInt(bigIntSecond.value, true);
                return tmp + bigIntFirst;
            }

            if (bigIntFirst < bigIntSecond && bigIntFirst.sign && bigIntSecond.sign)
            {
                BigInt tmp = bigIntSecond - bigIntFirst;
                tmp.ChangeSign(false);
                return tmp;
            }

            if (!bigIntFirst.sign && !bigIntSecond.sign)
            {
                if (bigIntFirst > bigIntSecond)
                {
                    BigInt tmp = (new BigInt(bigIntSecond.value, true)) - (new BigInt(bigIntFirst.value, true));
                    tmp.ChangeSign(true);
                    return tmp;
                }

                if (bigIntFirst < bigIntSecond)
                {
                    BigInt tmp = (new BigInt(bigIntSecond.value, true)) - (new BigInt(bigIntFirst.value, true));
                    tmp.ChangeSign(false);
                    return tmp;
                }
            }

            bool subSign = true;

            if (!bigIntFirst.sign && !bigIntSecond.sign)
            {
                subSign = false;
            }

            int k = 0;
            int n = Math.Max(bigIntFirst.value.Count, bigIntSecond.value.Count);

            List<int> sub = new List<int>();

            for (int i = 0; i < n; i++)
            {
                int tmpA = (bigIntFirst.value.Count > i) ? bigIntFirst.value[i] : 0;
                int tmpB = (bigIntSecond.value.Count > i) ? bigIntSecond.value[i] : 0;
                sub.Add(tmpA - tmpB - k);
                if (sub[i] < 0)
                {
                    sub[i] += b;
                    k = 1;
                }
                else
                {
                    k = 0;
                }
            }

            return new BigInt(normalize(sub), subSign);
        }

        public static BigInt operator *(BigInt bigIntFirst, int val)
        {
            int k = 0;
            List<int> mul = new List<int>();
            for (int i = 0; i < bigIntFirst.value.Count; i++)
            {
                long tmp = (long)bigIntFirst.value[i] * (long)val + k;
                mul.Add((int)(tmp % b));
                k = (int)(tmp / b);
            }
            mul.Add(k);

            bool f;
            if ((bigIntFirst.sign && val > 0) || (!bigIntFirst.sign && val < 0))
            {
                f = true;
            }
            else
            {
                f = false;
            }

            return new BigInt(normalize(mul), f);
        }

        public static BigInt operator *(BigInt bigIntFirst, BigInt bigIntSecond)
        {
            if (bigIntFirst == new BigInt("0") || bigIntSecond == new BigInt("0"))
            {
                return new BigInt("0");
            }

            BigInt mul = new BigInt("0");
            for (int i = 0; i < bigIntFirst.value.Count; i++)
            {
                BigInt temp = bigIntSecond * bigIntFirst.value[i];//умножаем текущий разряд числа на другое длинное число
                for (int j = 0; j < i; j++)//сдвигаем его
                {
                    temp.value.Insert(0, 0);
                }
                mul = mul + temp;//складываем с другими
            }

            bool f;
            if ((bigIntFirst.sign && bigIntSecond.sign) || (!bigIntFirst.sign && !bigIntSecond.sign))
            {
                f = true;
            }
            else
            {
                f = false;
            }

            return new BigInt(normalize(mul.value), f);
        }

        public static BigInt operator /(BigInt bigIntFirst, int val)
        {
            int[] q = new int[bigIntFirst.value.Count];
            int r = 0;
            int j = bigIntFirst.value.Count - 1;
            while (j >= 0)
            {
                long cur = (long)(r) * (long)(b) + bigIntFirst.value[j];
                q[j] = (int)(cur / val);
                r = (int)(cur % val);
                j--;
            }

            try
            {
                return new BigInt(q, bigIntFirst.sign);
            }
            catch
            {
                return new BigInt(new int[] { 0 }, bigIntFirst.sign);
            }
        }

        public static int operator %(BigInt bigIntFirst, int val)
        {
            int[] q = new int[bigIntFirst.value.Count];
            int r = 0;
            int j = bigIntFirst.value.Count - 1;
            while (j >= 0)
            {
                long cur = (long)(r) * (long)(b) + bigIntFirst.value[j];
                q[j] = (int)(cur / val);
                r = (int)(cur % val);
                j--;
            }

            return r;
        }

        public static BigInt operator /(BigInt bigIntFirst, BigInt bigIntSecond)
        {
            bool f;
            if ((bigIntFirst.sign && bigIntSecond.sign) || (!bigIntFirst.sign && !bigIntSecond.sign))
            {
                f = true;
            }
            else
            {
                f = false;
            }

            BigInt biFirst = new BigInt(bigIntFirst.value, true);
            BigInt biSecond = new BigInt(bigIntSecond.value, true);
            if (bigIntFirst < bigIntSecond)
            {
                return new BigInt(new int[] { 0 }, true);
            }

            //Начальная инициализация
            int n = bigIntSecond.value.Count;
            int m = bigIntFirst.value.Count - bigIntSecond.value.Count;
            int[] tempArray = new int[m + 1];
            tempArray[m] = 1;
            BigInt q = new BigInt(tempArray, true);

            //Нормализация
            int d = (b / (bigIntSecond.value[n - 1] + 1));
            biFirst = bigIntFirst * d;
            biFirst.ChangeSign(true);
            biSecond = bigIntSecond * d;
            biSecond.ChangeSign(true);
            if (biFirst.value.Count == n + m)//проверка на d==1
            {
                biFirst.value.Add(0);
            }
            //Начальная установка j
            int j = m;
            //Цикл по j
            while (j >= 0)
            {
                //Вычислить временное q 
                long cur = (long)(biFirst.value[j + n]) * (long)(b) + biFirst.value[j + n - 1];
                int tempq = (int)(cur / biSecond.value[n - 1]);
                int tempr = (int)(cur % biSecond.value[n - 1]);
                do
                {
                    if (tempq == b || biSecond.value.Count > 1 && (long)tempq * (long)biSecond.value[n - 2] > (long)b * (long)tempr + biFirst.value[j + n - 2])
                    {
                        tempq--;
                        tempr += biSecond.value[n - 1];
                    }
                    else
                    {
                        break;
                    }
                }
                while (tempr < b);
                //Умножить и вычесть
                BigInt bif2 = new BigInt(biFirst.value.GetRange(j, n + 1), true);
                bif2 = bif2 - (biSecond * tempq);
                bool flag = false;
                if (!bif2.sign)//если отрицательные
                {
                    flag = true;
                    List<int> bn = new List<int>();
                    for (int i = 0; i <= n; i++)
                    {
                        bn.Add(0);
                    }
                    bn.Add(1);
                    bif2.ChangeSign(true);
                    bif2 = (new BigInt(bn, true)) - bif2;
                }

                //Проверка остатка
                q.value[j] = tempq;
                if (flag)
                {
                    //Компенсировать сложение
                    q.value[j]--;
                    bif2 = bif2 + biSecond;
                    if (bif2.value.Count > n + j)
                    {
                        bif2.value.RemoveAt(n + j);
                    }

                }
                //Меняем u, так как все вычисления происходят с его разрядами
                for (int h = j; h < j + n; h++)
                {
                    if (h - j >= bif2.value.Count)
                    {
                        biFirst.value[h] = 0;
                    }
                    else
                    {
                        biFirst.value[h] = bif2.value[h - j];
                    }
                }
                j--;
            }
            q.value = normalize(q.value);
            //Денормализация
            //BigInt r = new BigInt(bif.value.GetRange(0, n), true) / d;

            return new BigInt(normalize(q.value), f);
        }

        public static BigInt operator %(BigInt bigIntFirst, BigInt bigIntSecond)
        {
            bool f;
            if ((bigIntFirst.sign && bigIntSecond.sign) || (bigIntFirst.sign && !bigIntSecond.sign))
            {
                f = true;
            }
            else
            {
                f = false;
            }

            BigInt biFirst = new BigInt(bigIntFirst.value, true);
            BigInt biSecond = new BigInt(bigIntSecond.value, true);
            if (bigIntFirst < bigIntSecond)
            {
                biFirst.ChangeSign(f);
                return biFirst;
            }

            //начальная инициализация
            int n = bigIntSecond.value.Count;
            int m = bigIntFirst.value.Count - bigIntSecond.value.Count;
            int[] tempArray = new int[m + 1];
            tempArray[m] = 1;
            BigInt q = new BigInt(tempArray, true);

            //Нормализация
            int d = (b / (bigIntSecond.value[n - 1] + 1));
            biFirst = bigIntFirst * d;
            biFirst.ChangeSign(true);
            biSecond = bigIntSecond * d;
            biSecond.ChangeSign(true);
            if (biFirst.value.Count == n + m)//проверка на d==1
            {
                biFirst.value.Add(0);
            }
            //Начальная установка j
            int j = m;
            //Цикл по j
            while (j >= 0)
            {
                //Вычислить временное q 
                long cur = (long)(biFirst.value[j + n]) * (long)(b) + biFirst.value[j + n - 1];
                int tempq = (int)(cur / biSecond.value[n - 1]);
                int tempr = (int)(cur % biSecond.value[n - 1]);
                do
                {
                    if (tempq == b || biSecond.value.Count > 1 && (long)tempq * (long)biSecond.value[n - 2] > (long)b * (long)tempr + biFirst.value[j + n - 2])
                    {
                        tempq--;
                        tempr += biSecond.value[n - 1];
                    }
                    else
                    {
                        break;
                    }
                }
                while (tempr < b);
                //Умножить и вычесть
                BigInt bif2 = new BigInt(biFirst.value.GetRange(j, n + 1), true);
                bif2 = bif2 - (biSecond * tempq);
                bool flag = false;
                if (!bif2.sign)//если отрицательные
                {
                    flag = true;
                    List<int> bn = new List<int>();
                    for (int i = 0; i <= n; i++)
                    {
                        bn.Add(0);
                    }
                    bn.Add(1);
                    bif2.ChangeSign(true);
                    bif2 = (new BigInt(bn, true)) - bif2;
                }

                //Проверка остатка
                q.value[j] = tempq;
                if (flag)
                {
                    //Компенсировать сложение
                    q.value[j]--;
                    bif2 = bif2 + biSecond;
                    if (bif2.value.Count > n + j)
                    {
                        bif2.value.RemoveAt(n + j);
                    }

                }
                //меняем u, так как все вычисления происходят с его разрядами
                for (int h = j; h < j + n; h++)
                {
                    if (h - j >= bif2.value.Count)
                    {
                        biFirst.value[h] = 0;
                    }
                    else
                    {
                        biFirst.value[h] = bif2.value[h - j];
                    }
                }
                j--;
            }
            q.value = normalize(q.value);

            //Денормализация
            BigInt r = new BigInt(biFirst.value.GetRange(0, n), f) / d;

            return r;
        }

        public static BigInt ModPow(BigInt bigIntFirst, BigInt bigIntSecond, BigInt MOD)
        {
            BigInt N = new BigInt(bigIntSecond);
            BigInt X = new BigInt("1");
            BigInt A = new BigInt(bigIntFirst);

            BigInt ZERO = new BigInt("0");
            BigInt ONE = new BigInt("1");
            BigInt TWO = new BigInt("2");

            while (N > ZERO)
            {
                int r = N % 2;
                BigInt q = N / TWO;

                if (r == 0)
                {
                    N = new BigInt(q);
                    A = (A * A) % MOD;
                }
                else
                {
                    N -= ONE;
                    X = (X * A) % MOD;
                }
            }
            return X;
        }
    }
}
