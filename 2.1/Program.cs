﻿using System;
using System.Numerics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2._1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("n = ");
            BigInteger n = BigInteger.Parse(Console.ReadLine());

            BigInteger z = SqRtN(SqRtN(n)) + 1;

            Console.WriteLine("z = " + z);

            BigInteger y = BigInteger.Pow(z, 2);

            Console.WriteLine("y = " + y);

            BigInteger t = n;

            Console.WriteLine("t = " + t);

            BigInteger idx = 1;

            for (BigInteger j = 1; j < z + 1; j++)
            {
                BigInteger yf = 1;

                BigInteger jz = j * z;

                for (BigInteger s = (j - 1) * z + 1; s < jz + 1; s++)
                {
                    yf *= s;
                }

                BigInteger answer = BigInteger.GreatestCommonDivisor(yf, t);

                if (answer != 1)
                {
                    idx = j;
                    break;
                }
            }

            BigInteger div1 = 1;

            BigInteger jj = idx;
            BigInteger jjz = jj * z;

            for (BigInteger i = (jj - 1) * z + 1; i < jjz + 1; i++)
            {
                if (t % i == 0 && i != 1)
                {
                    div1 = i;
                    break;
                }
            }

            BigInteger div2 = n / div1;

            Console.WriteLine(div1 + " * " + div2 + " = " + n);
        }

        public static BigInteger SqRtN(BigInteger N) //метод Нтютона
        {
            /*++
             *  Using Newton Raphson method we calculate the
             *  square root (N/g + g)/2
             */
            BigInteger rootN = N;
            int bitLength = 1; // There is a bug in finding bit length hence we start with 1 not 0
            while (rootN / 2 != 0)
            {
                rootN /= 2;
                bitLength++;
            }
            bitLength = (bitLength + 1) / 2;
            rootN = N >> bitLength;

            BigInteger lastRoot = BigInteger.Zero;
            do
            {
                lastRoot = rootN;
                rootN = (BigInteger.Divide(N, rootN) + rootN) >> 1;
            }
            while (!((rootN ^ lastRoot).ToString() == "0"));
            return rootN;
        } 
    }
}
