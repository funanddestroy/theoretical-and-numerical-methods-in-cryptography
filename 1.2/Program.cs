﻿using System;
using System.Numerics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1._2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("N = "); // N > 1
            BigInteger N = BigInteger.Parse(Console.ReadLine());
            Console.Write("R = "); // R > N, (R, N) = 1
            BigInteger R = BigInteger.Parse(Console.ReadLine());

            BigInteger[] gcd = Extended_GCD(R, N); // 0 < R' < N & RR' - NN' = 1
            BigInteger Ns = gcd[2];
            Ns = (Ns + N) % N;
            Console.WriteLine("N' = " + Ns);
            BigInteger Rs = gcd[1];
            Rs = (Rs + N) % N;
            Console.WriteLine("R' = " + Rs);

            Console.Write("T = "); // 0 < T < RN
            BigInteger T = BigInteger.Parse(Console.ReadLine());

            BigInteger m = T % R;
            m = (m * Ns) % R;
            BigInteger t = (T + m * N) / R;
            if (t >= N)
                Console.WriteLine(t - N);
            else
                Console.WriteLine(t);

            // (T * R') mod N
        }

        public static BigInteger[] Extended_GCD(BigInteger A, BigInteger B)
        {
            BigInteger[] result = new BigInteger[3];
            bool reverse = false;
            if (A < B)
            {
                BigInteger temp = A;
                A = B;
                B = temp;
                reverse = true;
            }
            BigInteger r = B;
            BigInteger q = 0;
            BigInteger x0 = 1;
            BigInteger y0 = 0;
            BigInteger x1 = 0;
            BigInteger y1 = 1;
            BigInteger x = 0, y = 0;
            while (A % B != 0)
            {
                r = A % B;
                q = A / B;
                x = x0 - q * x1;
                y = y0 - q * y1;
                x0 = x1;
                y0 = y1;
                x1 = x;
                y1 = y;
                A = B;
                B = r;
            }
            result[0] = r;
            if (reverse)
            {
                result[1] = y;
                result[2] = x;
            }
            else
            {
                result[1] = x;
                result[2] = y;
            }
            return result;
        }
    }
}
