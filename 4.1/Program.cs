﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4._1
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] lines = System.IO.File.ReadAllLines(@"..\..\input");

            double[] a = lines[0]
                    .Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(n => double.Parse(n))
                    .ToArray();
            double[] b = lines[1]
                    .Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(n => double.Parse(n))
                    .ToArray();
            double[] c = lines[2]
                    .Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(n => double.Parse(n))
                    .ToArray();
            double[] d = lines[3]
                    .Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(n => double.Parse(n))
                    .ToArray();

            int m = d.Length;

            double[] alfa = new double[m];
            double[] beta = new double[m];
            double[] gamma = new double[m];

            gamma[0] = b[0];
            alfa[0] = -(c[0] / gamma[0]);
            beta[0] = d[0] / gamma[0];

            for (int i = 1; i <= m - 2; i++)
            {
                gamma[i] = b[i] + a[i] * alfa[i - 1];
                alfa[i] = -(c[i] / gamma[i]);
                beta[i] = (d[i] - a[i] * beta[i - 1]) / gamma[i];
            }

            gamma[m - 1] = b[m - 1] + a[m - 1] * alfa[m - 2];
            beta[m - 1] = (d[m - 1] - a[m - 1] * beta[m - 2]) / gamma[m - 1];
            alfa[m - 1] = 0;

            double[] x = new double[m];

            x[m - 1] = beta[m - 1];

            for (int i = m - 2; i >= 0; i--)
            {
                x[i] = alfa[i] * x[i + 1] + beta[i];
            }

            for (int i = 0; i < m; i++)
            {
                Console.WriteLine("x[" + (i + 1) + "] = " + x[i]);
            }
        }
    }
}
