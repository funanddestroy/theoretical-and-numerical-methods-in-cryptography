﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4._3
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] lines = System.IO.File.ReadAllLines(@"..\..\input");

            int[] arr = lines[0]
                    .Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(n => int.Parse(n))
                    .ToArray();

            double[,] Matrix = new double[lines.Length, arr.Length];

            for (int i = 0; i < lines.Length; i++)
            {
                arr = lines[i]
                    .Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(n => int.Parse(n))
                    .ToArray();

                for (int j = 0; j < arr.Length; j++)
                {
                    Matrix[i, j] = arr[j];
                }
            }

            int size = lines.Length;

            double eps = 0.1;

            // Введем вектор значений неизвестных на предыдущей итерации,
            // размер которого равен числу строк в матрице, т.е. size,
            // причем согласно методу изначально заполняем его нулями
            //////vector < long double> previousVariableValues(size, 0.0);
            double[] previousVariableValues = new double[size];

            // Будем выполнять итерационный процесс до тех пор, 
            // пока не будет достигнута необходимая точность    
            while (true)
            {
                // Введем вектор значений неизвестных на текущем шаге       
                //////vector < long double> currentVariableValues(size);
                double[] currentVariableValues = new double[size];

                // Посчитаем значения неизвестных на текущей итерации
                // в соответствии с теоретическими формулами
                for (int i = 0; i < size; i++)
                {
                    // Инициализируем i-ую неизвестную значением 
                    // свободного члена i-ой строки матрицы
                    currentVariableValues[i] = Matrix[i, size];

                    // Вычитаем сумму по всем отличным от i-ой неизвестным
                    for (int j = 0; j < size; j++)
                    {
                        if (i != j)
                        {
                            currentVariableValues[i] -= Matrix[i, j] * previousVariableValues[j];
                        }
                    }

                    // Делим на коэффициент при i-ой неизвестной
                    currentVariableValues[i] /= Matrix[i, i];
                }

                // Посчитаем текущую погрешность относительно предыдущей итерации
                double error = 0.0;

                for (int i = 0; i < size; i++)
                {
                    error += Math.Abs(currentVariableValues[i] - previousVariableValues[i]);
                    Console.WriteLine(error);
                }
                Console.ReadKey();
                // Если необходимая точность достигнута, то завершаем процесс
                if (error < eps)
                {
                    break;
                }

                // Переходим к следующей итерации, так 
                // что текущие значения неизвестных 
                // становятся значениями на предыдущей итерации
                previousVariableValues = (double[])currentVariableValues.Clone();
            }

            for (int i = 0; i < previousVariableValues.Length; i++)
            {
                Console.WriteLine("x[" + (i + 1) + "] = " + previousVariableValues[i]);
            }
        }
    }
}
