﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4._2
{
    class Program
    {
        static uint RowCount;
        static uint ColumCount;
        static double[,] Matrix;
        static double[] RightPart;
        static double[] Answer;

        static void SortRows(int SortIndex)
        {

            double MaxElement = Matrix[SortIndex, SortIndex];
            int MaxElementIndex = SortIndex;
            for (int i = SortIndex + 1; i < RowCount; i++)
            {
                if (Matrix[i, SortIndex] > MaxElement)
                {
                    MaxElement = Matrix[i, SortIndex];
                    MaxElementIndex = i;
                }
            }

            //теперь найден максимальный элемент ставим его на верхнее место
            if (MaxElementIndex > SortIndex)//если это не первый элемент
            {
                double Temp;

                Temp = RightPart[MaxElementIndex];
                RightPart[MaxElementIndex] = RightPart[SortIndex];
                RightPart[SortIndex] = Temp;

                for (int i = 0; i < ColumCount; i++)
                {
                    Temp = Matrix[MaxElementIndex, i];
                    Matrix[MaxElementIndex, i] = Matrix[SortIndex, i];
                    Matrix[SortIndex, i] = Temp;
                }
            }
        }

        static int SolveMatrix()
        {
            if (RowCount != ColumCount)
                return 1; //нет решения

            for (int i = 0; i < RowCount - 1; i++)
            {
                SortRows(i);
                for (int j = i + 1; j < RowCount; j++)
                {
                    if (Matrix[i, i] != 0) //если главный элемент не 0, то производим вычисления
                    {
                        double MultElement = Matrix[j, i] / Matrix[i, i];
                        for (int k = i; k < ColumCount; k++)
                            Matrix[j, k] -= Matrix[i, k] * MultElement;
                        RightPart[j] -= RightPart[i] * MultElement;
                    }
                    //для нулевого главного элемента просто пропускаем данный шаг
                }
            }

            //ищем решение
            for (int i = (int)(RowCount - 1); i >= 0; i--)
            {
                Answer[i] = RightPart[i];

                for (int j = (int)(RowCount - 1); j > i; j--)
                    Answer[i] -= Matrix[i, j] * Answer[j];

                if (Matrix[i, i] == 0)
                    if (RightPart[i] == 0)
                        return 2; //множество решений
                    else
                        return 1; //нет решения

                Answer[i] /= Matrix[i, i];

            }
            return 0;
        }

        static void Main(string[] args)
        {
            string[] lines = System.IO.File.ReadAllLines(@"..\..\input");

            RightPart = lines[0]
                    .Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(n => double.Parse(n))
                    .ToArray();

            int[] arr = lines[1]
                    .Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(n => int.Parse(n))
                    .ToArray();

            Matrix = new double[lines.Length - 1, arr.Length];

            for (int i = 1; i < lines.Length; i++)
            {
                arr = lines[i]
                    .Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(n => int.Parse(n))
                    .ToArray();

                for (int j = 0; j < arr.Length; j++)
                {
                    Matrix[i - 1, j] = arr[j];
                }
            }

            RowCount = (uint)Matrix.GetLength(0);
            ColumCount = (uint)Matrix.GetLength(1);
            Answer = new double[RowCount];

            switch (SolveMatrix())
            {
                case 0:
                    for (int i = 0; i < Answer.Length; i++)
                    {
                        Console.WriteLine("x[" + (i + 1) + "] = " + Answer[i]);
                    }
                    break;
                case 1:
                    Console.WriteLine("Нет решений");
                    break;
                case 2:
                    Console.WriteLine("Множество решений");
                    break;
            }
        }
    }
}
